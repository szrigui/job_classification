import subprocess
import sys
import os.path
import pandas as pd
import numpy as np
from sklearn import metrics
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor
from sklearn.ensemble import RandomForestClassifier
import numpy as np
from random import randint
from math import inf


def clean_frame(dataframe):
    #print("clean_frame")
    #print(len(dataframe))
    l_f = np.array(dataframe['Run Time'])
    
    dataframe= dataframe.drop('Job_Number',axis = 1)
    dataframe= dataframe.drop('Wait Time',axis = 1)
    dataframe= dataframe.drop('premature',axis = 1)
    dataframe= dataframe.drop('Run Time',axis = 1)
    dataframe= dataframe.drop('Status',axis = 1)
    dataframe= dataframe.drop('date',axis = 1)   
    dataframe= dataframe.drop('Average CPU Time Used',axis = 1)
    dataframe=np.array(dataframe)
    #print(len(dataframe))
    return dataframe,l_f



read_folder = sys.argv[1]
write_folder = sys.argv[2]


read_path=sys.argv[1]
write_path =sys.argv[2]




limit = inf # variable to set the limit (to make the learning proccess go faster)

features=pd.read_csv(read_path)
features= features.sort_values(by=['Submit Time'])

cluster_start_time = features["Submit Time"].min()
features["Submit Time"]=features["Submit Time"] -cluster_start_time
features["week"]=features["Submit Time"]//604800
features

# predicting running time.

complete_predection_list=[]

print(len(features))
weekly_jobs=features.groupby(["week"])

for name,jobs in weekly_jobs:
    if name>limit:
        break
    
    print("week: ", name)
    print("data preperation")
    # extracting all the relevant data: jobs from past week and the history of the users 
    list_of_unique_users = pd.unique(jobs.User_ID)
    past_jobs=features.loc[features['week'] < name]

    if(len(past_jobs)<1):
        complete_predection_list=complete_predection_list+(jobs["Requested Time"].tolist())
        continue
    users_complete_history = past_jobs.loc[past_jobs['User_ID'].isin(list_of_unique_users)]
    users_complete_history_size=len(users_complete_history)
    learning_dataframe=pd.concat([users_complete_history,jobs],ignore_index= True)
    learning_dataframe_size=len(learning_dataframe)
    print(len(learning_dataframe))
    
    #learning
    print("learning")
    
    train_features,train_labels=clean_frame(users_complete_history)
    test_features,test_labels=clean_frame(jobs)
    rf= RandomForestRegressor(n_estimators= 100, random_state =randint(1,1000), n_jobs = -1)
    rf.fit(train_features,train_labels)
    predictions = rf.predict(test_features)

    complete_predection_list=complete_predection_list+list(predictions)
    print(metrics.explained_variance_score (test_labels,predictions))
    print('****************')

# when saving we retain the same format the prediction and premature columns are not used in the regression case 
# but we will keep them to maintain the unifomity of the code


dataframe=pd.read_csv(read_path)
dataframe= dataframe.sort_values(by=['Submit Time'])
dataframe["prediction"]=[-1]*len(dataframe)
dataframe["premature"]=[-1]*len(dataframe)
#dd=dataframe[['prediction',"premature"]]

# replace the user requested runtime with the predicted runtime
dataframe["Requested Time"]=[int(i) for i in complete_predection_list]
dataframe.to_csv(write_path,index = False)
