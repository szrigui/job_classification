#!/usr/bin/env bash


echo "splitting a trace into smaller parts"
type=$1
in_file=$2
out_file=$3

l=$(cat ${in_file} | grep -v ";" | wc -l | cut -d' ' -f1)
h=$((l/5))

echo "length: $h"

touch "${out_file}"

cat ${in_file} | grep ";" > "${out_file}"

if [ "$type" == "head" ]; then
  echo "getting the first part of the split"
  cat ${in_file} | grep -v ";" | head -n $h >> "${out_file}"
fi

if [ "$type" == "tail" ]; then
  echo "getting the last part of the split"
  cat ${in_file} | grep -v ";" | tail -n $h >> "${out_file}"
fi

if [ "$type" == "all" ]; then
	echo "getting the entire set"
	cat ${in_file} | grep -v ";" | tail -n $l >>"${out_file}" 
fi

