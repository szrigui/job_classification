#!/bin/bash
### trace: header_size separator
### SDSC-SP2: 48  1348
### SDSC-BLUE: 56 160 ?? 300
### CTC-SP2: 29 884
### KTH-SP2: 24  582
### METACENTRUM: 109


 ############################################## genration code ##############################################
original_file="workloads/original_traces/$trace_name.swf"
features_file="o_final_samples/temp/learning_features_$trace_name.csv"
learning_file="o_final_samples/temp/$trace_name-predction.csv"
prediction_file_ready="o_final_samples/temp/prediction_file_ready_$trace_name"


prediction_swf="o_final_samples/temp/simul_ready_$trace_name.swf"

prediction_json="o_final_samples/$trace_name_$sample.json"
prediction_json_perfect="o_final_samples/$trace_name_$sample.perfect.json"
prediction_json_idempotant="o_final_samples/$trace_name_$sample.idempotant.json"


learning_write_file="o_final_samples/$trace_name.RF"

#Rscript process_data.R $original_file $features_file $header_size $separator
#exit 0

#python3 RandomForestRegressor.py $features_file $learning_file | tee -a $learning_write_file 



Rscript concat_prediction_original.R $original_file $learning_file  $header_size $prediction_file_ready $separator

#add the header to the prediction file 

head -n $header_size $original_file  > $prediction_swf 
cat $prediction_file_ready >> $prediction_swf

# transfrom the file into a json format


echo "swf_to_json_delay"
python3 swf_to_json_delay.py -cs 100000000000 $prediction_swf $prediction_json -i 1 -th $separator
echo "swf_to_json_perfect"
python3 swf_to_json_perfect.py -cs 100000000000 $prediction_swf $prediction_json_perfect -i 1
 echo "swf_to_json_idempotent"
python3 swf_to_json_idempotance.py -cs 100000000000 $prediction_swf $prediction_json_idempotant -i 1 -th $separator

echo "data preperation finished !!!!"

exit 0






############################### remapeling code #############################

header_size="24"
trace_name='KTH-SP2'
separator="582"

original_file="workloads/original_traces/$trace_name.swf"
features_file="o_final_samples/temp/learning_features_$trace_name.csv"
learning_file="o_final_samples/temp/$trace_name-predction.csv"
prediction_file_ready="o_final_samples/temp/prediction_file_ready_$trace_name"


prediction_swf="o_final_samples/temp/simul_ready_$trace_name.swf"

#for sample in "1" "2" "3" "4" "5"
for sample in "6" "7" "8" "9" "10"
do
		echo "********************************************************************************************"
		echo "$sample"


		prediction_json="o_final_samples/$trace_name.$sample.json"
		prediction_json_perfect="o_final_samples/$trace_name.$sample.perfect.json"
		prediction_json_idempotant="o_final_samples/$trace_name.$sample.idempotant.json"


		learning_write_file="o_final_samples/$trace_name.RF"

		#Rscript process_data.R $original_file $features_file $header_size $separator
		#exit 0

		#python3 RandomForestRegressor.py $features_file $learning_file | tee -a $learning_write_file 



		Rscript concat_prediction_original.R $original_file $learning_file  $header_size $prediction_file_ready $separator 

		#add the header to the prediction file 

		head -n $header_size $original_file  > $prediction_swf 
		cat $prediction_file_ready >> $prediction_swf

		# transfrom the file into a json format


		echo "swf_to_json_delay"
		python3 swf_to_json_delay.py -cs 100000000000 $prediction_swf $prediction_json -i 1 -th $separator
		echo "swf_to_json_perfect"
		python3 swf_to_json_perfect.py -cs 100000000000 $prediction_swf $prediction_json_perfect -i 1
		 echo "swf_to_json_idempotent"
		python3 swf_to_json_idempotance.py -cs 100000000000 $prediction_swf $prediction_json_idempotant -i 1 -th $separator

		echo "data preperation finished !!!!"
done
exit 0





#finally run the simulations (not sure if this part should be included in this)
#python3 run_experiments_batsched.py  

