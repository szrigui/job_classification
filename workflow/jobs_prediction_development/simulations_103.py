import subprocess
import os.path
import time

multipilcation_factor=1
thresholds={
	'SDSC-SP2' : 129508, # TO FIX !!!!!!!!!!!!
	'CTC-SP2' : 129592,
	'KTH-SP2' : 432000,
	'SDSC-BLUE' : 721728,
	'meta_9' : 5400000,
	'meta_8' : 5400000,
	'ANL-Intr': 293400,
	'HPC2N':433200
}


#TRACES=['CTC-SP2','SDSC-BLUE','SDSC-SP2','KTH-SP2','meta_9','HPC2N'] # the traces
#POLICIES=['fcfs','saf','asc_walltime','asc_size', 'experimental'] # the policie we are using
#SAMPLES=["1","2","3","4","5"]
#SAMPLES=["6","7","8","9","10"]


#SAMPLES=["1","2","3","4","5"]
#POLICIES=['fcfs','saf','asc_walltime', 'experimental']
#states=["0","1","2","3","4","5"]

SAMPLES=["total"]

TRACES=["KTH-SP2",'SDSC-SP2']
POLICIES=['fcfs','saf','asc_walltime', 'experimental']
#states=["0","1","2","3","4","5"]
states=["0","1","2","4"]


states_traduction={
	"0": "normal",
	"1": "clairvoyant",
	"2": "prediction",
	"3": "idempotant",
	"4": "exact",
	"5": "regression"
}




# for every extention we generate the appropriate json file




check =False # just show the eventual files that will be generated without actually executing
results_folder = "o/results_test"
if not os.path.exists(results_folder+'/'):
	subprocess.call(['mkdir '+results_folder+'/'],shell=True)

json_workload_folder = "o/"




for sample in SAMPLES:
	if not os.path.exists(results_folder+'/'+ sample):
		subprocess.call(['mkdir '+results_folder+'/'+sample],shell=True)

	for trace in TRACES:
		# the results for each trace will be contained in a single folder (for simplicity)
		for policy in POLICIES:
			for state in states:
				state_name= states_traduction[state]

				# if the state is idempotent, exact or regression, we use specific extention to point at their particular workoald files 
				# and we change the state to use for the simulator

				if state == "3":
					extention = ".idempotant"
					state="2"
				else:
					extention = ".all"

				if state == "4":
					extention = ".exact"
					state="0"
				# elif state == "5":
				#  	state="0"


				with open("configurations/configuration_in.txt","w") as configuration_file:
					configuration_file.write("threshold " + str(thresholds[trace]*multipilcation_factor))
					configuration_file.write("\nstate " + str(state))

				
				if not os.path.exists(results_folder+'/'+ sample+'/'+ state_name):			
						subprocess.call(['mkdir '+results_folder+'/'+ sample+'/'+state_name],shell=True)

				if not os.path.exists(results_folder+'/'+ sample+'/'+state_name+'/'+trace):			
						subprocess.call(['mkdir '+results_folder+'/'+ sample+'/'+state_name+'/'+trace],shell=True)


				#result_file = results_folder+'_'+trace+'_'+policy+'_'+state_name+'_'+extention



				result_file = results_folder+'/'+ sample+'/'+state_name+'/'+trace+'/'+policy
				workload_file = json_workload_folder+trace+"."+sample+extention+".json"
				plateform_file = 'plateforms/'+ trace +'.xml'


				#print(extention, " :: " ,workload_file)
				print(result_file)

				#continue

				if(check == True):
					if (not(os.path.exists(result_file+"_jobs.csv"))):
						print(result_file+"_jobs.csv does not exist !!")
					continue

				#subprocess.call(['touch '+result_file],shell=True)

				if not os.path.exists(workload_file):
					print("workload_file :"+workload_file+" does not exist !!!")
				if not os.path.exists(plateform_file):
					print("plateform_file :"+plateform_file+" does not exist !!!")

				
				command=['robin generate ./expe.yaml'+
					' --output-dir=/tmp --ready-timeout=60'+
					' --batcmd="batsim -q -m master_host '+
					'--disable-schedule-tracing --disable-machine-state-tracing '+
					'-e "'+result_file+
					'" -p '+plateform_file+
					' -w '+workload_file+'"'+
					#'" -p plateforms/'+ trace +'.xml'+' -w o_post/idempotant/'+trace+'.json"'+
					' --schedcmd="batsched/build/batsched  -o '+policy+' -v easy_bf"']

				

				print("***************************************")
				print("sample "+sample+" ::" +state_name+" :: ",trace, " :: ",policy)
				print("***************************************")
				subprocess.call(command,shell=True)
				subprocess.call(['chmod 777 expe.yaml'],shell=True)
				subprocess.call(['robin ./expe.yaml'],shell=True)

				# print(trace,"::",current_state,"::",policy,'::',extention)

