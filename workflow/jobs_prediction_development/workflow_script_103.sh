#!/bin/bash
### trace: header_size separator
### SDSC-SP2: 48  1348
### SDSC-BLUE: 56 160 ?? 300
### CTC-SP2: 29 884
### KTH-SP2: 24  582
### 
### META_23: 109 500  METACENTRUM
### meta_9: 109 1000  METACENTRUM

### ANL-Intr 15 7000
### HPC2N 29  500

 ############################################## genration code ##############################################


# header_size="109"
# trace_name='meta_8' #the same as original trace name if there are no partitions
# original_trace_name='METACENTRUM' #the same as trace name if there are no partitions 
# separator="1000"
# partition="9"


header_size="56"
trace_name='SDSC-BLUE' #the same as original trace name if there are no partitions
original_trace_name='SDSC-BLUE' #the same as trace name if there are no partitions 
separator="160"
partition="-1"

original_file="workloads/original_traces/$original_trace_name.swf"


result_folder="o_proba"

features_file_classification="$result_folder/temp/learning_features_$trace_name.csv"
features_file_regression="$result_folder/temp/learning_features_regression_$trace_name.csv"

learning_file_classification="$result_folder/temp/$trace_name-classification.csv"
learning_file_regression="$result_folder/temp/$trace_name-regression.csv"
learning_file="$result_folder/temp/$trace_name.csv"

prediction_file_ready="$result_folder/temp/prediction_file_ready_$trace_name"


prediction_swf="$result_folder/temp/simul_ready_$trace_name.swf"


learning_write_file_classification="$result_folder/$trace_name-classification.RF"
learning_write_file_regression="$result_folder/$trace_name-regression.RF"
learning_write_file="$result_folder/$trace_name.RF"


#Rscript process_data.R $original_file $features_file $header_size $separator 


#Rscript process_data_meta.R $original_file $features_file_classification $header_size $separator $partition
#Rscript process_data_regression.R $original_file $features_file_regression $header_size $separator $partition







#python3 RF_classifier.py $features_file_classification $learning_file_classification | tee -a $learning_write_file_classification
#python3 RF_regressor.py $features_file_regression $learning_file_regression | tee -a $learning_write_file_regression


# merge the prediction into a single data file
#Rscript merge_classification_regression.R $learning_file_classification $learning_file_regression $learning_file
#
#exit 0
#for sample in "1" "2" "3"  "4" "5"
for sample in "year"
do
      echo "********************************************************************************************"
      echo "$sample"



      

      Rscript prepare_final_swf.R $original_file $learning_file  $header_size $prediction_file_ready $separator $partition $sample #2>log/merge.txt
      # #add the header to the prediction file 
      head -n $header_size $original_file  > "$prediction_swf.$sample" 
      cat $prediction_file_ready >> "$prediction_swf.$sample"


      python3 scripts/swf_to_json_regulated_predictions.py -cs 100000000000 "$prediction_swf.$sample" "$result_folder/$trace_name.$sample.all" -i 1

      #python3 scripts/swf_to_json.py -cs 100000000000 "$prediction_swf.$sample" "$result_folder/$trace_name.$sample.all" -i 1

      python3 scripts/swf_to_json_exact.py -cs 100000000000 "$prediction_swf.$sample" "$result_folder/$trace_name.$sample.exact" -i 1
      python3 scripts/swf_to_json_idempotant.py -cs 100000000000 "$prediction_swf.$sample" "$result_folder/$trace_name.$sample.idempotant" -i 1


      echo "data preperation finished !!!!"



      
done


