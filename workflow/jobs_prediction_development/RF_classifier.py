import subprocess
import sys
import os.path
import pandas as pd
import numpy as np
from sklearn import metrics
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor
from sklearn.ensemble import RandomForestClassifier
import numpy as np
from random import randint
from sklearn.cluster import DBSCAN
from math import inf



def clean_frame(dataframe):
	#print("clean_frame")
	#print(len(dataframe))
	l_f = np.array(dataframe['premature'])

	dataframe= dataframe.drop('Job_Number',axis = 1)
	dataframe= dataframe.drop('Wait Time',axis = 1)
	dataframe= dataframe.drop('premature',axis = 1)
	dataframe= dataframe.drop('Run Time',axis = 1)
	dataframe= dataframe.drop('Status',axis = 1)
	dataframe= dataframe.drop('date',axis = 1)   
	dataframe= dataframe.drop('Average CPU Time Used',axis = 1)
	#(list(dataframe))
	dataframe=np.array(dataframe)
	#print(len(dataframe))
	return dataframe,l_f



def main():

	read_path=sys.argv[1]
	write_path =sys.argv[2]

	limit = inf # variable to set the limit (to make the learning proccess go faster)

	features=pd.read_csv(read_path)
	features= features.sort_values(by=['Submit Time'])

	cluster_start_time = features["Submit Time"].min()
	features["Submit Time"]=features["Submit Time"] -cluster_start_time
	features["week"]=features["Submit Time"]//604800 #2592000


	pd.options.mode.chained_assignment = None 
	complete_predection_list=[]

	print(len(features))
	weekly_jobs=features.groupby(["week"])
	complete_divider_list = []
	complete_prediction_list =[]





	for name,jobs in weekly_jobs:
		if name>limit:
			break
		print("week: ", name)
		######################################################## prepare data ########################################################
		# extracting all the relevant data: jobs from past week and the history of the users 
		list_of_unique_users = pd.unique(jobs.User_ID)
		past_jobs=features.loc[features['week'] < name]

		if(len(past_jobs)<1):
			complete_predection_list=complete_predection_list+[-1]*len(jobs)
			complete_divider_list = complete_divider_list + [jobs["Run Time"].median()]*len(jobs)
			continue


		users_complete_history = past_jobs.loc[past_jobs['User_ID'].isin(list_of_unique_users)]
		users_complete_history_size=len(users_complete_history)
		learning_dataframe=pd.concat([users_complete_history,jobs],ignore_index= True)
		learning_dataframe_size=len(learning_dataframe)
		print(len(learning_dataframe))

		#determining the value of hte divider
		divider = users_complete_history["Run Time"].median()
		complete_divider_list = complete_divider_list + [divider] * len(jobs)

		print("divider: ", divider)

		users_complete_history["premature"] = 0
		users_complete_history.loc[users_complete_history["Run Time"]< divider, 'premature'] = 1
		jobs["premature"] = 0
		jobs.loc[jobs["Run Time"]< divider, 'premature'] = 1


		######################################################## learn ########################################################

		train_features,train_labels=clean_frame(users_complete_history)
		test_features,test_labels=clean_frame(jobs)
		clf= RandomForestClassifier(n_estimators =200,criterion="gini",max_depth=None,random_state=0,bootstrap=True)
		clf.fit(train_features,train_labels)


		predictions = clf.predict_proba(test_features)

		complete_predection_list = complete_predection_list + [k for k in predictions[:,1]]

		print("score: ",clf.score(test_features,test_labels))
		accuracy=metrics.accuracy_score(test_labels,[round(k) for k in predictions[:,1]])   
		F1=metrics.f1_score(test_labels,[round(k) for k in predictions[:,1]])
		Precision=metrics.precision_score(test_labels,[round(k) for k in predictions[:,1]])
		Recall=metrics.recall_score(test_labels,[round(k) for k in predictions[:,1]])

		print("distribution: ",accuracy)
		print("F1: ",F1)
		print("Precision: ",Precision)
		print("Recall: ",Recall)


		print("**************************")
	dataframe=pd.read_csv(read_path)
	dataframe= dataframe.sort_values(by=['Submit Time'])

	print(len(complete_predection_list))
	print(len(complete_divider_list))
	dataframe["prediction"]=complete_predection_list
	dataframe["divider"]=complete_divider_list
	dd=dataframe[['prediction',"premature","divider"]]
	print(sum(dd['premature'])/len(dd))
	print(sum(dd['prediction'])/len(dd))
	dataframe.to_csv(write_path,index = False)


if __name__ == '__main__':
	main()
	pass




# for name,jobs in weekly_jobs:
# 		if name>limit:
# 			break
# 		print("week: ", name)
# 		######################################################## prepare data ########################################################
# 		# extracting all the relevant data: jobs from past week and the history of the users 
# 		list_of_unique_users = pd.unique(jobs.User_ID)
# 		past_jobs=features.loc[features['week'] < name]

# 		if(len(past_jobs)<1):
# 			complete_predection_list=complete_predection_list+[-1]*len(jobs)
# 			complete_divider_list = complete_divider_list + [jobs["Run Time"].median()]*len(jobs)
# 			continue


# 		users_complete_history = past_jobs.loc[past_jobs['User_ID'].isin(list_of_unique_users)]
# 		users_complete_history_size=len(users_complete_history)
# 		learning_dataframe=pd.concat([users_complete_history,jobs],ignore_index= True)
# 		learning_dataframe_size=len(learning_dataframe)
# 		print(len(learning_dataframe))

# 		#determining the value of hte divider
# 		divider = users_complete_history["Run Time"].median()
# 		complete_divider_list = complete_divider_list + [divider] * len(jobs)

# 		print("divider: ", divider)

# 		users_complete_history["premature"] = 0
# 		users_complete_history.loc[users_complete_history["Run Time"]< divider, 'premature'] = 1
# 		jobs["premature"] = 0
# 		jobs.loc[jobs["Run Time"]< divider, 'premature'] = 1


# 		######################################################## learn ########################################################

# 		train_features,train_labels=clean_frame(users_complete_history)
# 		test_features,test_labels=clean_frame(jobs)
# 		clf= RandomForestClassifier(n_estimators =200,criterion="gini",max_depth=None,random_state=0,bootstrap=True)
# 		clf.fit(train_features,train_labels)


# 		predictions = clf.predict(test_features)
# 		complete_predection_list = complete_predection_list + list(predictions)
# 		#print(clf.feature_importances_)
# 		print("prediction stats:")
# 		rounded_predictions=np.copy(predictions)
# 		for j in range(len(predictions)):
# 			rounded_predictions[j]= 0 if predictions[j]<=0.5 else 1

# 		print("truth: ",sum(test_labels)/len(test_labels))
# 		distribution = sum(predictions)/len(predictions)
# 		accuracy=metrics.accuracy_score(test_labels,rounded_predictions)
# 		F1=metrics.f1_score(test_labels,rounded_predictions)
# 		Precision=metrics.precision_score(test_labels,rounded_predictions)
# 		Recall=metrics.recall_score(test_labels,rounded_predictions)

# 		print("distribution: ",distribution)
# 		print("accuracy: ",metrics.accuracy_score(test_labels,rounded_predictions))
# 		print("F1: ",metrics.f1_score(test_labels,rounded_predictions))
# 		print("Precision: ",metrics.precision_score(test_labels,rounded_predictions))
# 		print("Recall: ",metrics.recall_score(test_labels,rounded_predictions))
# 		print("**************************")
# 	dataframe=pd.read_csv(read_path)
# 	dataframe= dataframe.sort_values(by=['Submit Time'])

# 	print(len(complete_predection_list))
# 	print(len(complete_divider_list))
# 	dataframe["prediction"]=complete_predection_list
# 	dataframe["divider"]=complete_divider_list
# 	dd=dataframe[['prediction',"premature","divider"]]
# 	print(sum(dd['premature'])/len(dd))
# 	print(sum(dd['prediction'])/len(dd))
# 	dataframe.to_csv(write_path,index = False)


# if __name__ == '__main__':
# 	main()
# 	pass
			