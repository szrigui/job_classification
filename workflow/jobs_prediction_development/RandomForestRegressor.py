import subprocess
import sys
import os.path
import pandas as pd
import numpy as np
from sklearn import metrics
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor
from sklearn.ensemble import RandomForestClassifier
import numpy as np
from random import randint




read_folder = sys.argv[1]
write_folder = sys.argv[2]
#print(read_folder)
#print(write_folder)
#print (sys.argv)
#sys.exit()
#read_path='workloads/learning_data/learning_features_SDSC-SP2.csv'
#write_path ='workloads/learning_data/prediction_SDSC-SP2.csv'


read_path=sys.argv[1]
write_path =sys.argv[2]

features=pd.read_csv(read_path)
features= features.sort_values(by=['Submit Time'])
features_list = []
label_list = []

test_set_size = []
size=10000
print('preparing data sets')
for i in range(0,len(features),1000):
    print("prepare ",i)
    # take the most recent "size" jobs
    
    if i+10000 > len(features):
        f=features[i:len(features)]
        #features_list.append(f)
        #label_list.append(labels[i:len(features)])
    else:
        f=features[i:i+size]
        #label_list.append(labels[i:i+size])
   
    
    
    test_set_size.append(len(f)-9000)
    #take past data from previous users
    list_of_unique_users = pd.unique(f.User_ID)
    past_jobs=features[0:i]
    users_complete_history = past_jobs.loc[past_jobs['User_ID'].isin(list_of_unique_users)]
    f=pd.concat([users_complete_history,f],ignore_index= True)
    #print(len(users_complete_history),"::",len(f))
    
   
    l_f = np.array(f['premature'])
    
    f= f.drop('premature', axis = 1)
    f= f.drop('Job_Number',axis = 1)
    f= f.drop('Wait Time',axis = 1)
    f= f.drop('Run Time',axis = 1)
    f= f.drop('Status',axis = 1)
    f= f.drop('date',axis = 1)
    f= f.drop('Average CPU Time Used',axis = 1)
    f=np.array(f)
    features_list.append(f)
    label_list.append(l_f)
    
    if i+10000 > len(features):
        break    
        
len(pd.unique(features.User_ID))
not_learn = False
complete_predection_list =[]

score_list=[]
print("train and test phase")
for i in range(len(features_list)):
    window_size= len(features_list[i])   
    train_features=features_list[i][0:window_size-test_set_size[i]]
    test_features=features_list[i][window_size-test_set_size[i]:window_size]
    train_labels=label_list[i][0:window_size-test_set_size[i]]
    test_labels=label_list[i][window_size-test_set_size[i]:window_size]    
         
    print("-------\n",i)
    #print (len(train_features))
    #print (len(test_features))
    if (not_learn):
        continue
    #train_features, test_features, train_labels, test_labels=load_data_temporal_split(dd)
    clf= RandomForestClassifier(n_estimators =200,
                               criterion="gini",
                              max_depth=None,
                              random_state=0,
                               bootstrap=True)
    clf.fit(train_features,train_labels)
    predictions = clf.predict(test_features)
    #print(clf.feature_importances_)
    complete_predection_list = complete_predection_list + list(predictions)
    #print(len(complete_predection_list))
    rounded_predictions=np.copy(predictions)
    for j in range(len(predictions)):
        rounded_predictions[j]= 0 if predictions[j]<=0.5 else 1  
    #print("echlon: ",i)
    
    distribution = 1- sum(predictions)/len(predictions)
    accuracy=metrics.accuracy_score(test_labels,rounded_predictions)
    F1=metrics.f1_score(test_labels,rounded_predictions)
    Precision=metrics.precision_score(test_labels,rounded_predictions)
    Recall=metrics.recall_score(test_labels,rounded_predictions)
    
    score_list.append([distribution,accuracy,F1,Precision,Recall])
    score_list.append([distribution,accuracy,F1,Precision,Recall])

    print("truth: ",sum(test_labels)/len(test_labels))
    print("distribution: ",distribution)
    print("accuracy: ",metrics.accuracy_score(test_labels,rounded_predictions))
    print("F1: ",metrics.f1_score(test_labels,rounded_predictions))
    print("Precision: ",metrics.precision_score(test_labels,rounded_predictions))
    print("Recall: ",metrics.recall_score(test_labels,rounded_predictions))

complete_predection_list_complete = [-1]*9000 +complete_predection_list
print(len(complete_predection_list_complete))
print(len(features))


dataframe=pd.read_csv(read_path)
type(complete_predection_list_complete)
dataframe= dataframe.sort_values(by=['Submit Time'])
dataframe["prediction"]=complete_predection_list_complete
dd=dataframe[['prediction',"premature"]]
print(sum(dd['premature'])/len(dd))
print(sum(dd['prediction'])/len(dd))
dataframe.to_csv(write_path,index = False)



# def load_data_user_split(path):
    
#     features=pd.read_csv(path)
#     #features

#     labels = np.array(features['premature'])

#     features= features.drop('premature', axis = 1)


#     features= features.drop('Job_Number',axis = 1)
#     features= features.drop('Wait Time',axis = 1)
#     features= features.drop('Run Time',axis = 1)
#     features= features.drop('date',axis = 1)
#     feature_list = list(features.columns)

    
#     features= np.array(features)
#     return (features,labels)

# def load_data_temporal_split(path):
# 	features=pd.read_csv(path)
# 	labels = np.array(features['premature'])
# 	features= features.drop('premature', axis = 1)
# 	features= features.drop('Job_Number',axis = 1)
# 	features= features.drop('Wait Time',axis = 1)
# 	features= features.drop('Run Time',axis = 1)
# 	features= features.drop('date',axis = 1)

# 	train_features, test_features, train_labels, test_labels = train_test_split(features,labels,test_size = 0.5, random_state = 11, shuffle=False)
# 	return train_features, test_features, train_labels, test_labels 



# if __name__ == '__main__':

# # preparing the data
# 	trace_name = sys.argv[1]
# 	split_type=sys.argv[2]


# 	if(split_type == "user"):
# 		print("user split")
# 		train_features,train_labels  = load_data_user_split("learning_data/training_data_set_"+trace_name+".csv")
# 		test_features, test_labels = load_data_user_split("learning_data/testing_data_set_"+trace_name+".csv")
# 	if(split_type == "temporal"):
# 		print("temporal split")
# 		train_features, test_features, train_labels, test_labels=load_data_temporal_split("learning_data/testing_data_set_"+trace_name+".csv")



# #learning modal modal

# 	rf= RandomForestRegressor(n_estimators= 100, random_state =randint(1,1000), n_jobs = -1)
# 	rf.fit(train_features,train_labels)	
	

# 	predictions = rf.predict(test_features)


# 	rounded_predictions=np.copy(predictions)
# 	for i in range(len(predictions)):

# 		rounded_predictions[i]= 0 if predictions[i]<=0.5 else 1
# 		#print(test_labels[i],"::",predictions[i],"::",rounded_predictions[i])


# 	print("accuracy: ",metrics.accuracy_score(test_labels,rounded_predictions))
# 	print("F1: ",metrics.f1_score(test_labels,rounded_predictions))
# 	print("Precision: ",metrics.precision_score(test_labels,rounded_predictions))
# 	print("Recall: ",metrics.recall_score(test_labels,rounded_predictions))

# 	print ("Scores: ", rf.feature_importances_)

#  	