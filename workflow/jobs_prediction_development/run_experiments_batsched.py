import subprocess
import os.path
import time

#TRACES=['SDSC-BLUE-2000-4.2-cln','CTC-SP2-1996-3.1-cln','HPC2N-2002-2.2-cln','SDSC-SP2-1998-4.2-cln','Sandia-Ross-2001-1.1-cln','KTH-SP2-1996-2.1-cln']
#POLICIES=['fcfs','saf','asc_walltime','asc_size','desc_slowdown','experimental']

multipilcation_factor=3
thresholds={
	'SDSC-SP2' : 129508, # TO FIX !!!!!!!!!!!!
	'CTC-SP2' : 129592,
	'KTH-SP2' : 432000,
	'SDSC-BLUE' : 721728
}
states_traduction={
	"0": "normal",
	"1": "clairvoyant",
	"2": "prediction",
	"3": "idempotant",
	"4": "perfect",
	"5": "idem_all",
}

#TRACES=['KTH-SP2','CTC-SP2','SDSC-SP2','SDSC-BLUE']
#POLICIES=['fcfs','saf','asc_walltime','asc_size']
#states=["0","1","2","3","5"]


TRACES=['KTH-SP2','SDSC-SP2','CTC-SP2','SDSC-BLUE']
#POLICIES=['fcfs','saf','asc_walltime','asc_size', 'experimental']
POLICIES=['experimental']
states=["1"]
EXTENTIONS= ["_100","_median_10000","_median_all","_mean", "_extra"]

start=time.time()

#results_folder="results_test/"

results_folder="results_subset_thresholds/results/"
#subprocess.call(['rm -rf '+results_folder+'/'],shell=True)
if not os.path.exists(results_folder+'/'):
	subprocess.call(['mkdir '+results_folder+'/'],shell=True)


# we start executing the exprimental
check =False
extention= ""

for trace in TRACES:
	for state in states:
		current_state = states_traduction[state]
		for policy in POLICIES:
			
			for extention in EXTENTIONS:
				print(trace,"::",current_state,"::",policy,'::',extention)
				if not os.path.exists(results_folder+'/'+trace):			
					subprocess.call(['mkdir '+results_folder+'/'+trace],shell=True)

				if not os.path.exists(results_folder+'/'+trace+'/'+ current_state):			
					subprocess.call(['mkdir '+results_folder+'/'+trace+'/'+current_state],shell=True)

				if not os.path.exists(results_folder+'/'+trace+'/'+current_state+'/'+policy):			
					subprocess.call(['mkdir '+results_folder+'/'+trace+'/'+current_state+'/'+policy],shell=True)


				with open("configurations/configuration_in.txt","w") as configuration_file:
					configuration_file.write("threshold " + str(thresholds[trace]*multipilcation_factor)) 
					configuration_file.write("\nstate " + str(state))

				if (check):
					continue

				command=['robin generate ./expe.yaml'+
					' --output-dir=/tmp --ready-timeout=60'+
					' --batcmd="batsim -q -m master_host --disable-schedule-tracing --disable-machine-state-tracing -e "'+results_folder+'/'+trace+'/'+current_state+'/'+policy+'/'+policy+extention+'_'+
					'" -p plateforms/'+ trace +'.xml'+' -w results_subset_thresholds/'+trace+extention+'.json"'+
					#'" -p plateforms/'+ trace +'.xml'+' -w o/idempotant/'+trace+'.json"'+
					' --schedcmd="batsched/build/batsched  -o '+policy+' -v easy_bf"']
				subprocess.call(command,shell=True)
				subprocess.call(['chmod 777 expe.yaml'],shell=True)
				subprocess.call(['robin ./expe.yaml'],shell=True)
				print(','.join([trace,policy,extention]))
			#/home/salah/expriements/predicting_failed_jobs/batsim_simgrid_new_env/workloads/delay
			#quit()		

end=time.time()
with open("configurations/time ","a") as time_file:
	time_file.write("\n"+str(end-start))

