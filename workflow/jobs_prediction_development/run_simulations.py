import subprocess
import os.path
import time

multipilcation_factor=1
thresholds={
	'SDSC-SP2' : 129508, # TO FIX !!!!!!!!!!!!
	'CTC-SP2' : 129592,
	'KTH-SP2' : 432000,
	'SDSC-BLUE' : 721728,
	'meta_9' : 5400000,
	'meta_8' : 5400000,
	'ANL-Intr': 293400
}


#TRACES=['CTC-SP2','SDSC-BLUE','SDSC-BLUE'] # the traces
#POLICIES=['fcfs','saf','asc_walltime','asc_size', 'experimental'] # the policie we are using
#states=["0","4","1","2","3"]


TRACES=['ANL-Intr']
POLICIES=['saf','asc_walltime','fcfs','experimental']
states=["0","2"]

states_traduction={
	"0": "normal",
	"1": "clairvoyant",
	"2": "prediction",
	"3": "idempotant",
	"4": "perfect",
}


#states=["0","4","1","2","3"]


separator =""

# for every extention we generate the appropriate json file




check =True # just show the eventual files that will be generated without actually executing
results_folder = "o_final_cluster/results"
if not os.path.exists(results_folder+'/'):
	subprocess.call(['mkdir '+results_folder+'/'],shell=True)


json_workload_folder = "o_final_cluster/"



for trace in TRACES:
	# the results for each trace will be contained in a single folder (for simplicity)


	for policy in POLICIES:
		for state in states:
			state_name= states_traduction[state]

			

			if state in ["1","2","3"]:
				state_name= state_name+separator
			# if the state is perfect or the state is idempotant we use different traces
			if state == "3":
				extention = ".idempotant"
				state="2"
			elif state == "4":
				extention = ".perfect"
				state="0"
			else:
				extention = ""

		
			with open("configurations/configuration_in.txt","w") as configuration_file:
				configuration_file.write("threshold " + str(thresholds[trace]*multipilcation_factor))
				configuration_file.write("\nstate " + str(state))

			
			if not os.path.exists(results_folder+'/'+ state_name):			
					subprocess.call(['mkdir '+results_folder+'/'+state_name],shell=True)

			if not os.path.exists(results_folder+'/'+state_name+'/'+trace):			
					subprocess.call(['mkdir '+results_folder+'/'+state_name+'/'+trace],shell=True)


			#result_file = results_folder+'_'+trace+'_'+policy+'_'+state_name+'_'+extention



			result_file = results_folder+'/'+state_name+'/'+trace+'/'+policy
			workload_file = json_workload_folder+trace+extention+".json"
			plateform_file = 'plateforms/'+ trace +'.xml'


			#subprocess.call(['touch '+result_file],shell=True)

			if not os.path.exists(workload_file):
				print("workload_file :"+workload_file+" does not exist !!!")
			if not os.path.exists(plateform_file):
				print("plateform_file :"+plateform_file+" does not exist !!!")

			
			command=['robin generate ./expe.yaml'+
				' --output-dir=/tmp --ready-timeout=60'+
				' --batcmd="batsim -q -m master_host '+
				'--disable-schedule-tracing --disable-machine-state-tracing '+
				'-e "'+result_file+
				'" -p '+plateform_file+
				' -w '+workload_file+'"'+
				#'" -p plateforms/'+ trace +'.xml'+' -w o/idempotant/'+trace+'.json"'+
				' --schedcmd="batsched/build/batsched  -o '+policy+' -v easy_bf"']

			

			print("***************************************")
			print(state_name+" :: ",trace, " :: ",policy)
			print("***************************************")
			subprocess.call(command,shell=True)
			subprocess.call(['chmod 777 expe.yaml'],shell=True)
			subprocess.call(['robin ./expe.yaml'],shell=True)

			# print(trace,"::",current_state,"::",policy,'::',extention)

