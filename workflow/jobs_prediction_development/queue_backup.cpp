#include "queue.hpp"

#include <vector>
#include <cmath>

#include <boost/algorithm/string.hpp>

#include "pempek_assert.hpp"
#include <iostream>
#include <fstream>
#include <valarray>
using namespace std;

extern double threshold;
extern int state;// 0 = none, 1 = premature, 2 = prediction


void SortableJob::update_slowdown(Rational current_date)
{
    waiting_time = current_date - release_date;

	slowdown = (waiting_time + job->walltime) / job->walltime;
}

void SortableJob::update_bounded_slowdown(Rational current_date, Rational execution_time_lower_bound)
{
    bounded_slowdown = (current_date -  release_date) / execution_time_lower_bound;
}


bool FCFSOrder::compare(const SortableJob *j1, const SortableJob *j2, const SortableJobOrder::CompareInformation *info) const
{
    (void) info;
//       std::ofstream outfile;

//    outfile.open("did_it_get_through.txt", std::ios_base::app);
//    outfile << "clairvoyant" << j1->job->clairvoyant << "\n" <<std::endl; 
//    outfile << "premature" << j1->job->premature << "\n" <<std::endl;
//    outfile << "prediction" << j1->job->prediction << "\n" <<std::endl;
//    outfile.close();
    

//    std::ofstream outfile;
//    outfile.open("configurations/configuration_out.txt", std::ios_base::app);
//    outfile <<" the threshold: "<< thresh << "\n the state: "<< etat <<std::endl; 
//    outfile.close();
      
    if(state == 1){
        if (j1->job->premature ==1){
            return true;
        }     
    }
    if(state == 2){
        if (j1->job->prediction>= 0.5){
            return true;
        }   
    }
    
    if (j1->release_date == j2->release_date)
        return j1->job->id < j2->job->id;
    else
        return j1->release_date < j2->release_date;
}

void FCFSOrder::updateJob(SortableJob *job, const SortableJobOrder::UpdateInformation *info) const
{
    (void) job;
    (void) info;
}


bool LCFSOrder::compare(const SortableJob *j1, const SortableJob *j2, const SortableJobOrder::CompareInformation *info) const
{
    (void) info;
    
    if(j1->waiting_time >= threshold){
    	if(j2->waiting_time >= threshold){
    		return j1->release_date < j2->release_date;
    	}
    	return true;
    }

    if (j1->release_date == j2->release_date)
        return j1->job->id < j2->job->id;
    else
        return j1->release_date > j2->release_date;
}

void LCFSOrder::updateJob(SortableJob *job, const SortableJobOrder::UpdateInformation *info) const
{
    (void) job;
    (void) info;
}


bool DescendingBoundedSlowdownOrder::compare(const SortableJob *j1, const SortableJob *j2, const SortableJobOrder::CompareInformation *info) const
{
    (void) info;
    if(j1->waiting_time >= threshold){
    	if(j2->waiting_time >= threshold){
    		return j1->release_date < j2->release_date;
    	}
    	return true;
    }
    if (j1->bounded_slowdown == j2->bounded_slowdown)
        return j1->job->id < j2->job->id;
    else
        return j1->bounded_slowdown > j2->bounded_slowdown;
}

void DescendingBoundedSlowdownOrder::updateJob(SortableJob *job, const SortableJobOrder::UpdateInformation *info) const
{
    job->update_bounded_slowdown(info->current_date, _min_job_length);
}


bool DescendingSlowdownOrder::compare(const SortableJob *j1, const SortableJob *j2, const SortableJobOrder::CompareInformation *info) const
{
    (void) info;
    
    if(j1->waiting_time >= threshold){
    	if(j2->waiting_time >= threshold){
    		return j1->release_date < j2->release_date;
    	}
    	return true;
    }
    
    if (j1->slowdown == j2->slowdown)
        return j1->job->id < j2->job->id;
    else
        return j1->slowdown > j2->slowdown;
}

void DescendingSlowdownOrder::updateJob(SortableJob *job, const SortableJobOrder::UpdateInformation *info) const
{
    job->update_slowdown(info->current_date);
}


bool AscendingSizeOrder::compare(const SortableJob *j1, const SortableJob *j2, const SortableJobOrder::CompareInformation *info) const
{
    (void) info;
    
    if(state == 1){
        if (j1->job->premature ==1){
            return true;
        }     
    }
    if(state == 2){
        if (j1->job->prediction>= 0.5){
            return true;
        }     
    }
    
    if(j1->waiting_time >= threshold){
    	if(j2->waiting_time >= threshold){
    		return j1->release_date < j2->release_date;
    	}
    	return true;
    }
    
    if (j1->job->nb_requested_resources == j2->job->nb_requested_resources)
        return j1->job->id < j2->job->id;
    else
        return j1->job->nb_requested_resources < j2->job->nb_requested_resources;
}

void AscendingSizeOrder::updateJob(SortableJob *job, const SortableJobOrder::UpdateInformation *info) const
{
    (void) job;
    (void) info;
}


bool DescendingSizeOrder::compare(const SortableJob *j1, const SortableJob *j2, const SortableJobOrder::CompareInformation *info) const
{
    (void) info;
    if(j1->waiting_time >= threshold){
    	if(j2->waiting_time >= threshold){
    		return j1->release_date < j2->release_date;
    	}
    	return true;
    }

    if (j1->job->nb_requested_resources == j2->job->nb_requested_resources)
        return j1->job->id < j2->job->id;
    else
        return j1->job->nb_requested_resources > j2->job->nb_requested_resources;
}

void DescendingSizeOrder::updateJob(SortableJob *job, const SortableJobOrder::UpdateInformation *info) const
{
    (void) job;
    (void) info;
}


bool AscendingWalltimeOrder::compare(const SortableJob *j1, const SortableJob *j2, const SortableJobOrder::CompareInformation *info) const
{
    (void) info;
    if(state == 1){
        if (j1->job->premature ==1){
            return true;
        }     
    }
    if(state == 2){
        if (j1->job->prediction>= 0.5){
            return true;
        }     
    }
    
    if(j1->waiting_time >= threshold){
    	if(j2->waiting_time >= threshold){
    		return j1->release_date < j2->release_date;
    	}
    	return true;
    }

    if (j1->job->walltime == j2->job->walltime)
        return j1->job->id < j2->job->id;
    else
        return j1->job->walltime < j2->job->walltime;
}

void AscendingWalltimeOrder::updateJob(SortableJob *job, const SortableJobOrder::UpdateInformation *info) const
{
    (void) job;
    (void) info;
}


bool DescendingWalltimeOrder::compare(const SortableJob *j1, const SortableJob *j2, const SortableJobOrder::CompareInformation *info) const
{
    (void) info;
    if(j1->waiting_time >= threshold){
    	if(j2->waiting_time >= threshold){
    		return j1->release_date < j2->release_date;
    	}
    	return true;
    }
    if (j1->job->walltime == j2->job->walltime)
        return j1->job->id < j2->job->id;
    else
        return j1->job->walltime > j2->job->walltime;
}

void DescendingWalltimeOrder::updateJob(SortableJob *job, const SortableJobOrder::UpdateInformation *info) const
{
    (void) job;
    (void) info;
}

bool F2Order::compare(const SortableJob *j1, const SortableJob *j2, const SortableJobOrder::CompareInformation *info) const
{
    (void) info;

    //F2
    //double j1_score = (sqrt((double) j1->job->walltime) * j1->job->nb_requested_resources)
    //		+ (2.56e+4 * log10(j1->job->submission_time));

    //double j2_score = (sqrt((double) j2->job->walltime) * j2->job->nb_requested_resources)
    //	    		+ (2.56e+4 * log10(j2->job->submission_time));

    //new data with waiting time
	double j1_score = (0.001555542128 * log10((double) j1->job->walltime)) +
			(0.000474521850699 * sqrt((double) j1->job->nb_requested_resources)) +
			(0.00601020863739 * log10((double) j1->waiting_time));

	double j2_score = (0.001555542128 * log10((double) j2->job->walltime)) +
			(0.000474521850699 * sqrt((double) j2->job->nb_requested_resources)) +
			(0.00601020863739 * log10((double) j2->waiting_time));

    double threshold = 129508.0*3.0;
    if(j1->waiting_time >= threshold){
    	if(j2->waiting_time >= threshold){
    		return j1->release_date < j2->release_date;
    	}
    	return true;
    }

    if (j1_score == j2_score)
        return j1->job->id < j2->job->id;
    else
        return j1_score < j2_score;
}

void F2Order::updateJob(SortableJob *job, const SortableJobOrder::UpdateInformation *info) const
{
    job->waiting_time = info->current_date - (Rational) job->job->submission_time;
    //(void) info;
}



bool Experimental_order::compare(const SortableJob *j1, const SortableJob *j2, const SortableJobOrder::CompareInformation *info) const
{
    
  //in this experiment we see how much we can gain by executing the failing jobs earlier
    (void) info;
    if(j1->job->walltime>1000 && j1->job->runtime <100)//if the job is destined to fail then just give it a priority
        return true;
     if (j1->release_date == j2->release_date)
        return j1->job->id < j2->job->id;
    else
        return j1->release_date < j2->release_date;
    
//    (void) info;
//    //j1->release_date
//    double j1_area_mod = (((double) j1->job->walltime) * j1->job->nb_requested_resources)/sqrt((double) j1->waiting_time);
//
//    double j2_area_mod = (((double) j2->job->walltime) * j2->job->nb_requested_resources)/sqrt((double) j2->waiting_time);
//
//
//    if (j1_area_mod == j2_area_mod)
//        return j1->job->id < j2->job->id;
//    else
//        return j1_area_mod < j2_area_mod;
}

void Experimental_order::updateJob(SortableJob *job, const SortableJobOrder::UpdateInformation *info) const
{
    job->waiting_time = info->current_date - (Rational) job->job->submission_time;
    //(void) info;
}






bool ShortestAreaOrder::compare(const SortableJob *j1, const SortableJob *j2, const SortableJobOrder::CompareInformation *info) const
{
    (void) info;
    if(state == 1){
        if (j1->job->premature ==1){
            return true;
        }     
    }
    if(state == 2){
        if (j1->job->prediction>= 0.5){
            return true;
        }   
    }
    if(j1->waiting_time >= threshold){
    	if(j2->waiting_time >= threshold){
    		return j1->release_date < j2->release_date;
    	}
    	return true;
    }
    double j1_area = ((double) j1->job->walltime) * j1->job->nb_requested_resources;

    double j2_area = ((double) j2->job->walltime) * j2->job->nb_requested_resources;


    if (j1_area == j2_area)
        return j1->job->id < j2->job->id;
    else
        return j1_area < j2_area;
}

void ShortestAreaOrder::updateJob(SortableJob *job, const SortableJobOrder::UpdateInformation *info) const
{
    //(void) job;
    (void) info;
    job->waiting_time = info->current_date - (Rational) job->job->submission_time;
}

bool ProtoOrder::compare(const SortableJob *j1, const SortableJob *j2, const SortableJobOrder::CompareInformation *info) const
{
    (void) info;
    double ALPHA = 0.33333333;
    double BETA = 0.9;
    double GAMMA = -1.16666667;
    double A = -0.16666667;

    //(pow((double) j1->job->walltime,0.33333333)*pow(j1->job->nb_requested_resources,0.9)) + (-0.16666667*(pow(j1->job->submission_time,-1.16666667)));
    double j1_score = (pow((double) j1->job->walltime,ALPHA)*pow(j1->job->nb_requested_resources,BETA)) + (A*(pow(j1->job->submission_time,GAMMA)));

    double j2_score = (pow((double) j2->job->walltime,ALPHA)*pow(j2->job->nb_requested_resources,BETA)) + (A*(pow(j2->job->submission_time,GAMMA)));


    if (j1_score == j2_score)
        return j1->job->id < j2->job->id;
    else
        return j1_score < j2_score;
}

void ProtoOrder::updateJob(SortableJob *job, const SortableJobOrder::UpdateInformation *info) const
{
    //(void) job;
    (void) info;
    job->waiting_time = info->current_date - (Rational) job->job->submission_time;
}

/**********
** QUEUE **
**********/

Queue::Queue(SortableJobOrder *order) :
    _order(order)
{

}

Queue::~Queue()
{
    auto it = _jobs.begin();

    while (it != _jobs.end())
        it = remove_job((*it)->job);
}

void Queue::append_job(const Job *job, SortableJobOrder::UpdateInformation *update_info)
{
    SortableJob * sjob = new SortableJob;
    sjob->job = job;
    sjob->release_date = update_info->current_date;

    _jobs.push_back(sjob);
}

std::list<SortableJob *>::iterator Queue::remove_job(const Job *job)
{
    auto it = std::find_if(_jobs.begin(), _jobs.end(),
                           [job](SortableJob * sjob)
                           {
                               return sjob->job == job;
                           });

    PPK_ASSERT_ERROR(it != _jobs.end(), "Cannot remove job '%s': not in the queue", job->id.c_str());
    return remove_job(it);
}

std::list<SortableJob *>::iterator Queue::remove_job(std::list<SortableJob *>::iterator job_it)
{
    SortableJob * sjob = *job_it;
    delete sjob;

    return _jobs.erase(job_it);
}

void Queue::sort_queue(SortableJobOrder::UpdateInformation *update_info,
                       SortableJobOrder::CompareInformation *compare_info)
{
    // Update of all jobs
    for (SortableJob * sjob : _jobs)
        _order->updateJob(sjob, update_info);

    // Sort
    _jobs.sort([this, compare_info](const SortableJob * j1, const SortableJob * j2)
                {
                    return _order->compare(j1, j2, compare_info);
    });
}

const Job* Queue::first_job() const
{
    PPK_ASSERT_ERROR(_jobs.size() > 0, "No first job: Queue is empty");
    return (*_jobs.begin())->job;
}

const Job *Queue::first_job_or_nullptr() const
{
    if (_jobs.size() == 0)
        return nullptr;
    else
        return first_job();
}

bool Queue::contains_job(const Job *job) const
{
    auto it = std::find_if(_jobs.begin(), _jobs.end(),
                           [job](SortableJob * sjob)
                           {
                               return sjob->job == job;
                           });
    return it != _jobs.end();
}

bool Queue::is_empty() const
{
    return _jobs.size() == 0;
}

int Queue::nb_jobs() const
{
    return _jobs.size();
}

Rational Queue::compute_load_estimation() const
{
    Rational load = 0;

    for (auto queue_it = _jobs.begin(); queue_it != _jobs.end(); ++queue_it)
    {
        const SortableJob * sjob = *queue_it;
        const Job * job = sjob->job;

        load += job->nb_requested_resources * job->walltime;
    }

    return load;
}

std::string Queue::to_string() const
{
    vector<string> jobs_strings;
    jobs_strings.reserve(_jobs.size());

    for (const SortableJob * sjob : _jobs)
        jobs_strings.push_back(sjob->job->id);

    return "[" + boost::algorithm::join(jobs_strings, ", ") + "]";
}

std::list<SortableJob *>::iterator Queue::begin()
{
    return _jobs.begin();
}

std::list<SortableJob *>::iterator Queue::end()
{
    return _jobs.end();
}

std::list<SortableJob *>::const_iterator Queue::begin() const
{
    return _jobs.begin();
}

std::list<SortableJob *>::const_iterator Queue::end() const
{
    return _jobs.end();
}

