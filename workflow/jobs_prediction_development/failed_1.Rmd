---
title: "Iteration 2"
output: html_notebook
---


The goal of this is to find a deviding line that  seperate the small form the normal jobs in a way that truly improve performance

```{r}
library(ggplot2)
library(reshape2)
library(dplyr)
library(viridis)
library(scales)
library(tidyr)
library(ggrepel)#for the labelling on the image
library(data.table)
library(directlabels)
library(RColorBrewer)
library(xtable)
### SDSC-SP2: 48
### SDSC-BLUE: 56
### CTC-SP2: 29
### KTH-SP2: 24

header_SDSC_SP2 = 48
header_SDSC_BLUE = 56
header_CTC_SP2 = 29
header_KTH_SP2 = 24
read_original_swf <-function(path, header_size=0){
  d=read.csv(path,header = FALSE,skip = header_size,sep="")
  names(d)=c("Job_Number","Submit Time","Wait Time","Run Time","Number of Allocated Processors","Average CPU Time Used","Used Memory","Requested Number of  Processors","Requested Time","Requested Memory","Status","User_ID","Group_ID","Executable (Application) Number","Queue Number","Partition Number","Preceding Job Number","Think Time from Preceding Job")
  d=d%>%filter(`Number of Allocated Processors`>0 & Status==1 &`Requested Time`>0)
  return(d)
  
}


```


```{r}
d_KTH_original=read_original_swf("workloads/original_traces/KTH-SP2.swf",header_size = header_KTH_SP2)
d_SDSD_SP2_original=read_original_swf("workloads/original_traces/SDSC-SP2.swf",header_size = header_SDSC_SP2)
d_SDSD_BLUE_original=read_original_swf("workloads/original_traces/SDSC-BLUE.swf",header_size = header_SDSC_BLUE)
d_CTC_original=read_original_swf("workloads/original_traces/CTC-SP2.swf",header_size = header_CTC_SP2)

```
```{r}

ggplot(d_KTH_original)+geom_point(aes(x=log(`Run Time`),y=log(`Run Time`)))

```
```{r}
select_seperator <-function(d_original){
  # ggplot(d_original,aes(`Run Time`))+
  # geom_density()+scale_y_continuous(name = "%", labels=scales::percent)+xlim(1, 10000)+
  # geom_bar(stat="bin", aes(y=..density..)) +
  # geom_vline(aes(xintercept = 100),color="blue")+
  # geom_vline(aes(xintercept = median(d_original$`Run Time`)),color="red")


  print("size:")
  print(nrow(d_original))
  print("mean:")
  print(mean(d_original$`Run Time`))
  print("median:")
  print(median(d_original$`Run Time`))
  print("Q1:")
  print(quantile(d_original$`Run Time`))
}
print("------------------------------------------KTH-SP2------------------------------------------------")
select_seperator(d_KTH_original)
print("--------------------------")
select_seperator(head(d_KTH_original%>%arrange(`Submit Time`),10000))
print("-------------------------------------------------------------------------------------------------")
print("------------------------------------------CTC-SP2------------------------------------------------")
select_seperator(d_CTC_original)
print("--------------------------")
select_seperator(head(d_CTC_original%>%arrange(`Submit Time`),10000))
print("-------------------------------------------------------------------------------------------------")
print("------------------------------------------SDSC-SP2------------------------------------------------")
select_seperator(d_SDSD_SP2_original)
print("--------------------------")
select_seperator(head(d_SDSD_SP2_original%>%arrange(`Submit Time`),10000))
print("-------------------------------------------------------------------------------------------------")
print("------------------------------------------SDSC-BLUE------------------------------------------------")
select_seperator(d_SDSD_BLUE_original)
print("--------------------------")
select_seperator(head(d_SDSD_BLUE_original%>%arrange(`Submit Time`),10000))
print("-------------------------------------------------------------------------------------------------")

```
```{r}
compare_thresholds<- function(trace_path){
  
    #policies=c("fcfs","saf","asc_walltime","asc_size")
    policies=c("saf","fcfs","asc_walltime","asc_size","experimental")
    #methods= c("normal","clairvoyant","prediction","idempotant","idem_all")
    methods= c("clairvoyant")
    thresholds = c("_100","_mean","_median_all","_median_10000","_extra")
    d=NULL
    for (policy in policies){
      for (method in methods){
        for (threshold in thresholds){
           file = paste0(trace_path,method,"/",policy,"/",policy,threshold,"__jobs.csv")
            print(file)
            
            d_temp=read.csv(file)%>%arrange(submission_time)
            d_temp=d_temp %>%filter(profile != "dummy")
            d_temp$method= method
            d_temp$policy=policy
            if(threshold == ''){
              d_temp$threshold="none"
            }else{
              d_temp$threshold =threshold
            }
            
            d_temp$week=d_temp$starting_time%/%604800
            d_temp$bounded_stretch=pmax((d_temp$waiting_time+d_temp$execution_time)/pmax(d_temp$execution_time,60),1)
            d_temp$bounded_pp_stretch=(d_temp$waiting_time+d_temp$execution_time)/pmax(d_temp$execution_time,60)
                    d_temp$bounded_pp_stretch =pmax(((d_temp$waiting_time+d_temp$execution_time)/ (pmax(d_temp$execution_time,60)*d_temp$requested_number_of_resources)),1)
                    
            d_temp$cumulative_stretch=cumsum(d_temp$stretch)
            d_temp$cumulative_waiting_time=cumsum(d_temp$waiting_time)
            d_temp$cumulative_bounded_stretch=cumsum(d_temp$bounded_stretch)
            d_temp$cumulative_bounded_pp_stretch=cumsum(d_temp$bounded_pp_stretch)
            
            d_temp=d_temp%>% select(-workload_name,-profile,-success,-final_state, -starting_time, -finish_time, -turnaround_time,-metadata,
                                    -allocated_resources, -consumed_energy)
            #print(d_temp)
            if(is.null(d)){ d=d_temp }
            else{ d=rbind(d,d_temp) }
          }
    }
          
        }
       
    d$policy[d$policy == "asc_size"] <- "SQF"
    d$policy[d$policy == "asc_walltime"] <- "SPF"
    d$policy[d$policy == "saf"] <- "SAF"
    d$policy[d$policy == "fcfs"] <- "FCFS"
    d$policy[d$policy == "experimental"] <- "WFP"
    return (d)
}
extract_perfect_normal <- function(trace_path){
  
    policies=c("fcfs","saf","asc_walltime","asc_size","experimental")
    #policies=c("saf","asc_walltime")
    #methods= c("normal","clairvoyant","prediction","idempotant","idem_all")
    methods= c("normal","perfect")
    d=NULL
    for (policy in policies){
      for (method in methods){
        file = paste0(trace_path,method,"/",policy,"/",policy,"__jobs.csv")
        print(file)
        d_temp=read.csv(file)%>%arrange(submission_time)
        d_temp=d_temp %>%filter(profile != "dummy")
        d_temp$method= method
        d_temp$policy=policy
        d_temp$threshold= method
        d_temp$week=d_temp$starting_time%/%604800
        d_temp$bounded_stretch=pmax((d_temp$waiting_time+d_temp$execution_time)/pmax(d_temp$execution_time,60),1)
        d_temp$bounded_pp_stretch =pmax(((d_temp$waiting_time+d_temp$execution_time)/ (pmax(d_temp$execution_time,60)*d_temp$requested_number_of_resources)),1)
        
        d_temp$cumulative_stretch=cumsum(d_temp$stretch)
        d_temp$cumulative_waiting_time=cumsum(d_temp$waiting_time)
        d_temp$cumulative_bounded_stretch=cumsum(d_temp$bounded_stretch)
        d_temp$cumulative_bounded_pp_stretch=cumsum(d_temp$bounded_pp_stretch)
        
        d_temp=d_temp%>% select(-workload_name,-profile,-success,-final_state,-starting_time,-finish_time,-turnaround_time,-metadata,-allocated_resources,-consumed_energy)
        #print(d_temp)
        if(is.null(d)){ d=d_temp }
        else{ d=rbind(d,d_temp) }
      }
    }
    d$policy[d$policy == "asc_size"] <- "SQF"
    d$policy[d$policy == "asc_walltime"] <- "SPF"
    d$policy[d$policy == "saf"] <- "SAF"
    d$policy[d$policy == "fcfs"] <- "FCFS"
    d$policy[d$policy == "experimental"] <- "WFP"
    return (d)
}
d_SDSC_SP2_pn=extract_perfect_normal(trace_path = "results_subset_2/SDSC-SP2/")
d_SDSC_SP2_thresholds=compare_thresholds(trace_path = "results_subset_thresholds/results/SDSC-SP2/")
d_SDSC_SP2_thresholds=rbind(d_SDSC_SP2_thresholds,d_SDSC_SP2_pn)
d_SDSC_SP2_thresholds$trace="SDSC-SP2"


d_CTC_SP2_pn=extract_perfect_normal(trace_path = "results_subset_2/CTC-SP2/")
d_CTC_SP2_thresholds=compare_thresholds(trace_path = "results_subset_thresholds/results/CTC-SP2/")
d_CTC_SP2_thresholds=rbind(d_CTC_SP2_thresholds,d_CTC_SP2_pn)
d_CTC_SP2_thresholds$trace="CTC-SP2"



d_SDSC_BLUE_pn=extract_perfect_normal(trace_path = "results_subset_2/SDSC-BLUE/")
d_SDSC_BLUE_thresholds=compare_thresholds(trace_path = "results_subset_thresholds/results/SDSC-BLUE/")
d_SDSC_BLUE_thresholds=rbind(d_SDSC_BLUE_thresholds,d_SDSC_BLUE_pn)
d_SDSC_BLUE_thresholds$trace="SDSC-BLUE"
```
```{r fig.width=20, fig.height= 30}
d_all_thresholds=rbind(d_SDSC_SP2_thresholds,d_CTC_SP2_thresholds,d_SDSC_BLUE_thresholds)
ggplot()+
  geom_line(data=d_all_thresholds,aes(x=submission_time/604800,y=cumulative_bounded_stretch,color=threshold))+
  facet_wrap(~policy+trace,scales = "free" ,ncol=3) 
# ggplot()+
#   geom_line(data=d_SDSC_SP2_thresholds,aes(x=submission_time/604800,y=cumulative_bounded_stretch,color=threshold))+
#   facet_wrap(~trace+policy,scales = "free" ,nrow=2) +
#   xlab("Week")#+ylab("Cumulative Bounded Stretch")+ggsave("/home/salah/expriements/predicting_failed_jobs/documentation/report raf/figures/threshold_changes_SDSC_SP2.pdf",width = 20, height = 20)
# 
# 
# 
# ggplot()+
#   geom_line(data=d_CTC_SP2_thresholds,aes(x=submission_time/604800,y=cumulative_bounded_stretch,color=threshold))+
#   facet_wrap(~trace+policy,scales = "free" ,nrow=2) +
#   xlab("Week")#+ylab("Cumulative Bounded Stretch")+ggsave("/home/salah/expriements/predicting_failed_jobs/documentation/report raf/figures/threshold_changes_CTC_SP2.pdf",width = 20, height = 20)
# 
# 
# 
# ggplot()+
#   geom_line(data=d_SDSC_BLUE_thresholds,aes(x=submission_time/604800,y=cumulative_bounded_stretch,color=threshold))+
#   facet_wrap(~trace+policy,scales = "free" ,nrow=2) +
#   xlab("Week")#+ylab("Cumulative Bounded Stretch")+ggsave("/home/salah/expriements/predicting_failed_jobs/documentation/report raf/figures/threshold_changes_SDSC_BLUE.pdf",width = 20, height = 20)
```
```{r}
d_SDSC_BLUE_thresholds%>%group_by(policy,threshold)%>%
  summarise(mean_bounded_stretch=mean(bounded_stretch))%>% 
  filter(policy== "FCFS")%>%arrange(mean_bounded_stretch)%>%
  mutate(mean_bounded_stretch_norm=(mean_bounded_stretch-max(mean_bounded_stretch))/max(mean_bounded_stretch))
``` 

```{r}
d_SDSC_BLUE_thresholds%>%group_by(policy,threshold)%>%filter(bounded_stretch ==max(bounded_stretch))%>%
  select(job_id, requested_number_of_resources, requested_time,threshold,policy,bounded_stretch)%>%arrange(policy)
```

```{r}
d_temp=d_SDSC_BLUE_thresholds%>%filter(policy == "SAF")%>%
  group_by(threshold)%>%
  summarise(sum_bounded_stretch=sum(bounded_stretch))%>%
  arrange(sum_bounded_stretch,)

d_temp
#spread(d_temp, key =method, value=mean_stretch)
```

```{r}
plot(density(d_KTH_original$`Number of Allocated Processors`))
plot(density(d_CTC_original$`Number of Allocated Processors`))
plot(density(d_SDSD_SP2_original$`Number of Allocated Processors`))
plot(density(d_SDSD_BLUE_original$`Number of Allocated Processors`))
```
```{r}

extract_stats <- function(path, name ,header,separator){
  d_temp=read_original_swf("workloads/original_traces/SDSC-BLUE.swf",header_SDSC_BLUE)
  d_temp$class="normal"
  d_temp$class[d_temp$`Run Time`<separator]="small"
  d_temp$`Run Time`=d_temp$`Run Time`/3600
  d_temp$trace=name
  d_temp=d_temp%>%group_by(trace,class)%>%
    summarise(number=n(),runtimes=sum(`Run Time`),areas=sum(`Run Time`*`Number of Allocated Processors`))%>%
    group_by(trace)%>%
    mutate(number=number/sum(number),runtimes=runtimes/sum(runtimes),areas=areas/sum(areas))
  return (d_temp)
  
}
```
```{r}
SDSC_SP2_separator= 1348
CTC_SP2_separator= 884
SDSC_BLUE_sparator= 300

d_temp =rbind(extract_stats(path="workloads/original_traces/SDSC-SP2.swf",name="SDSC-SP2",header=header_SDSC_SP2,separator = SDSC_SP2_separator),
              extract_stats(path="workloads/original_traces/CTC-SP2.swf",name="CTC-SP2",header=header_CTC_SP2,separator = CTC_SP2_separator),
              extract_stats(path="workloads/original_traces/SDSC-BLUE.swf",name="SDSC-BLUE",header=header_SDSC_BLUE,separator = SDSC_BLUE_sparator)
              )


```
```{r}
xtable(d_temp)
```


