#!/bin/bash




############################ generating KTH trace with diffrent thresholds ###########################################################################
echo "changing the threshold"

### SDSC-SP2: 48
### SDSC-BLUE: 56
### CTC-SP2: 29
### KTH-SP2: 24
### METACENTRUM: 109



header_size="24"
trace_name='KTH-SP2'

prediction_swf="o/prediction_file_ready_$trace_name"
prediction_json="results_subset_thresholds/$trace_name"

python3 swf_to_json_batsim_change_threshold.py -cs 100000000000 "$prediction_swf" "$prediction_json"_100.json -i -1 -th 100
python3 swf_to_json_batsim_change_threshold.py -cs 100000000000 "$prediction_swf" "$prediction_json"_median_10000.json -i -1 -th 382 #median_10000
python3 swf_to_json_batsim_change_threshold.py -cs 100000000000 "$prediction_swf" "$prediction_json"_median_all.json -i -1 -th 582 #median_all
python3 swf_to_json_batsim_change_threshold.py -cs 100000000000 "$prediction_swf" "$prediction_json"_mean.json -i -1 -th 6135 #mean (maybe_not)
python3 swf_to_json_batsim_change_threshold.py -cs 100000000000 "$prediction_swf" "$prediction_json"_extra.json -i -1 -th 100 #extra





header_size="48"
trace_name='SDSC-SP2'

prediction_swf="o/prediction_file_ready_$trace_name"
prediction_json="results_subset_thresholds/$trace_name"

python3 swf_to_json_batsim_change_threshold.py -cs 100000000000 "$prediction_swf" "$prediction_json"_100.json -i -1 -th 100
python3 swf_to_json_batsim_change_threshold.py -cs 100000000000 "$prediction_swf" "$prediction_json"_median_10000.json -i -1 -th 1348 #median_10000
python3 swf_to_json_batsim_change_threshold.py -cs 100000000000 "$prediction_swf" "$prediction_json"_median_all.json -i -1 -th 347 #median_all
python3 swf_to_json_batsim_change_threshold.py -cs 100000000000 "$prediction_swf" "$prediction_json"_mean.json -i -1 -th 6616 #mean (maybe_not)
python3 swf_to_json_batsim_change_threshold.py -cs 100000000000 "$prediction_swf" "$prediction_json"_extra.json -i -1 -th 700 #extra


header_size="29"
trace_name='CTC-SP2'

prediction_swf="o/prediction_file_ready_$trace_name"
prediction_json="results_subset_thresholds/$trace_name"

python3 swf_to_json_batsim_change_threshold.py -cs 100000000000 "$prediction_swf" "$prediction_json"_100.json -i -1 -th 100
python3 swf_to_json_batsim_change_threshold.py -cs 100000000000 "$prediction_swf" "$prediction_json"_median_10000.json -i -1 -th 884 #median_10000
python3 swf_to_json_batsim_change_threshold.py -cs 100000000000 "$prediction_swf" "$prediction_json"_median_all.json -i -1 -th 883 #median_all
python3 swf_to_json_batsim_change_threshold.py -cs 100000000000 "$prediction_swf" "$prediction_json"_mean.json -i -1 -th 8735 #mean (maybe_not)
python3 swf_to_json_batsim_change_threshold.py -cs 100000000000 "$prediction_swf" "$prediction_json"_extra.json -i -1 -th 500 #extra



header_size="56"
trace_name='SDSC-BLUE'

prediction_swf="o/prediction_file_ready_$trace_name"
prediction_json="results_subset_thresholds/$trace_name"

python3 swf_to_json_batsim_change_threshold.py -cs 100000000000 "$prediction_swf" "$prediction_json"_100.json -i -1 -th 100
python3 swf_to_json_batsim_change_threshold.py -cs 100000000000 "$prediction_swf" "$prediction_json"_median_10000.json -i -1 -th 86 #median_10000
python3 swf_to_json_batsim_change_threshold.py -cs 100000000000 "$prediction_swf" "$prediction_json"_median_all.json -i -1 -th 160 #median_all
python3 swf_to_json_batsim_change_threshold.py -cs 100000000000 "$prediction_swf" "$prediction_json"_mean.json -i -1 -th 3186 #mean (maybe_not)
python3 swf_to_json_batsim_change_threshold.py -cs 100000000000 "$prediction_swf" "$prediction_json"_extra.json -i -1 -th 800 #extra




# python3 swf_to_json_batsim_change_threshold.py -cs 100000000000 o/prediction_file_ready_KTH-SP2 results_subset_thresholds/KTH-SP2_100.json -i -1 -th 100
# python3 swf_to_json_batsim_change_threshold.py -cs 100000000000 o/prediction_file_ready_KTH-SP2 results_subset_thresholds/KTH-SP2_median_10000.json -i -1 -th 100 #median_10000
# python3 swf_to_json_batsim_change_threshold.py -cs 100000000000 o/prediction_file_ready_KTH-SP2 results_subset_thresholds/KTH-SP2_median_all.json -i -1 -th 100 #median_all
# python3 swf_to_json_batsim_change_threshold.py -cs 100000000000 o/prediction_file_ready_KTH-SP2 results_subset_thresholds/KTH-SP2_mean.json -i -1 -th 100 #mean (maybe_not)
# python3 swf_to_json_batsim_change_threshold.py -cs 100000000000 o/prediction_file_ready_KTH-SP2 results_subset_thresholds/KTH-SP2_100.json -i -1 -th 100


############################ extracting the subset for a trace ###########################################################################



# header_size="24"
# trace_name='KTH-SP2'
# original_file="workloads/original_traces/$trace_name.swf"
# features_file="o/learning_features_$trace_name.csv"
# learning_file="o/$trace_name-predction.csv"
# prediction_file_ready="o_KTH-SP2-ALL/prediction_file_ready_$trace_name"
# prediction_swf="o_KTH-SP2-ALL/simul_ready_$trace_name.swf"
# prediction_json="o_KTH-SP2-ALL/$trace_name-4.json"


# Rscript concat_prediction_original.R $original_file $learning_file  $header_size $prediction_file_ready

# head -n $header_size $original_file  > $prediction_swf 
# cat $prediction_file_ready >> $prediction_swf


# python3 swf_to_json_batsim_delay.py -cs 100000000000 $prediction_swf $prediction_json -i 1




############################## generating the idempotence cases #################################################################


# echo "idempotance"
# python3 swf_to_json_idempotance.py -cs 100000000000 o/prediction_file_ready_KTH-SP2 o/KTH-SP2_idempotant.json -i 1
# python3 swf_to_json_idempotance.py -cs 100000000000 o/prediction_file_ready_SDSC-SP2 o/SDSC-SP2_idempotant.json -i 1
# python3 swf_to_json_idempotance.py -cs 100000000000 o/prediction_file_ready_CTC-SP2 o/CTC-SP2_idempotant.json -i 1
# python3 swf_to_json_idempotance.py -cs 100000000000 o/prediction_file_ready_SDSC-BLUE o/SDSC-BLUE_idempotant.json -i 1


# echo " "
# echo "full idem\n"
# echo " "
# python3 swf_to_json_idem_all.py -cs 100000000000 o/prediction_file_ready_KTH-SP2 o/KTH-SP2_idem_all.json -i 1
# python3 swf_to_json_idem_all.py -cs 100000000000 o/prediction_file_ready_SDSC-SP2 o/SDSC-SP2_idem_all.json -i 1
# python3 swf_to_json_idem_all.py -cs 100000000000 o/prediction_file_ready_CTC-SP2 o/CTC-SP2_idem_all.json -i 1
# python3 swf_to_json_idem_all.py -cs 100000000000 o/prediction_file_ready_SDSC-BLUE o/SDSC-BLUE_idem_all.json -i 1



