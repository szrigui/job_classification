#!/usr/bin/env bash
#usage: strong_filter_all_swf
#output inplace
file="$1"
output="$2"
partition="$3"

echo strong_filtering $file

#print header
awk '($1==";"){print}' $file > $output


#remove header for treatment
awk '!($1==";"){print}' $file |


#requested_cores and allocated_cores management: if all strictly positive dont touch, if one is, take it for both, and if none are, do not print.
awk '{
if ($8<=0 && $5<=0)
  p=43;
else if ($8>0 && $5<=0)
  {$5 = $8;
  print;}
else if ($5>0 && $8<=0)
  {$8 = $5;
  print;}
else
  print;
}' |

#runtime >0
awk '($4>0){print}' |

#time_req to runtime if time_req<=0
awk '{
if ($9<=0)
  p=43
else
  print;
}' |

#force runtime and reqtimes to at least 1
awk '{
if ($9<1)
  { $9=1;
    print; }
else
  print;
}
'|

awk '{
if ($4<1)
  { $4=1;
    print; }
else
  print;
}
'|


#if req_time < run_time
awk '{
if ($9<$4){p=43}
else
  print;
}' |

awk -v par=$partition '($16==par){print}' |

#sub_time >0
awk '($2>=0){print}' >> $output
