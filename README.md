# Overview
This repository has two main directories:

- ``workflow`` which contains the experimental workflow used to generate the data (learning and simulation).
- ``analysis`` which contains the analysis and the visulaization notebook used to generate the figure

## Installation
First, install [nix](https://nixos.org/nix/) package manager

Then, clone this nix repository which contains all the packages need to run the experiments.
```{sh} 
git clone https://gitlab.inria.fr/szrigui/job_classification.git
```

## Experiments:
``Loading the nix environment``: The following command will install all the software and the dependencies needed the run the experiments
```{sh} 
nix-shell ./datamovepkgs/ -A batsched
```
Two script must be executed to obtain the results:

- First we prepare the traces and transform into a json format ( input for the simulator)
```{sh}
bash -e prepare_data.sh
```
- execute the simulations for 6 trace and 4 policies and 5 methods
```{sh}
python3 simulations.py
```





## Visualization

- The data is stored in a folder named [data](./analysis/data/)
- The Figures can  be re-produced using the scripts in this   [notebook](./analysis/notebook.pdf)






